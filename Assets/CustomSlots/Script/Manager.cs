﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CSFramework
{
    public class Manager : MonoBehaviour 

    {
        public CustomSlot[] m_customSlots;
        public static Manager Instance;
        // Start is called before the first frame update
        public void Awake()
        {
            Instance = this;
        }
        private void Start()
        {
            foreach(CustomSlot cm in m_customSlots)
            {
                cm.Play();
                Debug.Log("---called ----    ::  " + cm);
            }
          
            
        }
    }

}